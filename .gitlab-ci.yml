# GitLab CI configuration file
#
# To run this locally (requires `gitlab-runner`):
#   $ gitlab-runner exec docker <job_name>

image: docker:latest

stages:
  - lint
  - build
  - release

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  PROD_STACK_NAME: archive
  REVIEW_STACK_NAME: archive-review
  IMAGE: recalbox/archive

# Lint

.lint_template: &lint_definition
  stage: lint
  only:
    - master
    - branches
  tags:
    - lintstage

lint config-files:
  <<: *lint_definition
  before_script:
    - apk add --update --no-cache libxml2-utils
  script: test/lint/config-files

lint buildroot:
  <<: *lint_definition
  image: python:2-alpine
  before_script:
    - pip install six
  script: test/lint/buildroot

lint commits:
  <<: *lint_definition
  image: node:14.11
  before_script:
    - npm install -g @commitlint/cli @commitlint/config-conventional
  script: test/lint/commits
  allow_failure: true

# Build

.build_template: &build_definition
  stage: build
  before_script:
    - source scripts/release/functions.sh
  script:
    - export RECALBOX_VERSION="${CI_COMMIT_TAG:-${CI_COMMIT_REF_SLUG:0:12} (${CI_COMMIT_SHA:0:8}) ${CI_PIPELINE_ID} ${ARCH} $(date '+%Y/%m/%d %H:%M:%S')}"
    - cp "${YOUTUBE_API_KEYS}" "./api_keys.json"
    - docker build -t "recalbox-${ARCH}" .
    - docker run --rm -v `pwd`:/work -v /recalbox-builds/dl:/share/dl -v "/recalbox-builds/ccaches/ccache-${ARCH}:/share/ccache" -e "ARCH=${ARCH}" -e "RECALBOX_VERSION=${RECALBOX_VERSION}" -e "RECALBOX_CCACHE_ENABLED=true" "recalbox-${ARCH}" 2>&1 | tee build.log | grep '>>>' || tac build.log | grep '>>>' -m 1 -B 9999 | tac
    - rm "./api_keys.json"
    - export DIST_DIR="dist/${ARCH}"
    - mkdir -p "${DIST_DIR}"
    - cp output/images/recalbox/* "${DIST_DIR}"
    - rm -rf output/
    - echo "${RECALBOX_VERSION}" >> "${DIST_DIR}/recalbox.version"
    - cp CHANGELOG.md "${DIST_DIR}/recalbox.changelog"
    - uploadFolderToBucket "${AWS_ACCESS_KEY_ID}" "${AWS_SECRET_ACCESS_TOKEN}" "recalbox-builds-artifacts" "dist" "${CI_PIPELINE_ID}"
  after_script:
    - rm -rf buildroot
  artifacts:
    name: dist-${ARCH}-${CI_PIPELINE_ID}
    when: always
    paths:
      - build.log
    expire_in: 2 mos

build rpi1:
  <<: *build_definition
  when: manual
  only:
    - master
    - tags
  variables:
    ARCH: 'rpi1'
build rpi2:
  <<: *build_definition
  when: manual
  only:
    - master
    - tags
  variables:
    ARCH: 'rpi2'
build rpi3:
  <<: *build_definition
  when: manual
  only:
    - master
    - tags
  variables:
    ARCH: 'rpi3'
build rpi4:
  <<: *build_definition
  when: manual
  only:
    - master
    - tags
  variables:
    ARCH: 'rpi4'
build x86_64:
  <<: *build_definition
  when: manual
  only:
    - master
    - tags
  variables:
    ARCH: 'x86_64'
build x86:
  <<: *build_definition
  when: manual
  only:
    - master
    - tags
  variables:
    ARCH: 'x86'
build odroidxu4:
  <<: *build_definition
  when: manual
  only:
    - master
    - tags
  variables:
    ARCH: 'odroidxu4'

build rpi1 for test:
  <<: *build_definition
  when: manual
  only:
    - branches
  except:
    - master
  variables:
    ARCH: 'rpi1'
build rpi2 for test:
  <<: *build_definition
  when: manual
  only:
    - branches
  except:
    - master
  variables:
    ARCH: 'rpi2'
build rpi3 for test:
  <<: *build_definition
  when: manual
  only:
    - branches
  except:
    - master
  variables:
    ARCH: 'rpi3'
build rpi4 for test:
  <<: *build_definition
  when: manual
  only:
    - branches
  except:
    - master
  variables:
    ARCH: 'rpi4'
build x86_64 for test:
  <<: *build_definition
  when: manual
  only:
    - branches
  except:
    - master
  variables:
    ARCH: 'x86_64'
build x86 for test:
  <<: *build_definition
  when: manual
  only:
    - branches
  except:
    - master
  variables:
    ARCH: 'x86'
build odroidxu4 for test:
  <<: *build_definition
  when: manual
  only:
    - branches
  except:
    - master
  variables:
    ARCH: 'odroidxu4'

### Release

.release_template: &release_definition
  stage: release
  variables:
    DEPLOY_TYPE: type of the deployment (prod, review)
    SKIP_IMAGES: Release or without images if true
    BUCKET: Bucket to deploy to
    BUCKET_PATH: path in the bucket
  before_script:
    - source scripts/release/functions.sh
    - apk add --update-cache --no-cache xz bash
  script:
    - downloadBucketFolderTo "${AWS_ACCESS_KEY_ID}" "${AWS_SECRET_ACCESS_TOKEN}" "recalbox-builds-artifacts" "builds" "${CI_PIPELINE_ID}"
    - mkdir release
    - ls -la builds
    - ./scripts/release/generate_external_installer_assets.sh --images-dir builds --destination-dir release
    - echo "Releasing files"
    - mv builds/* release
    - if [ "${DEPLOY_TYPE}" == "prod" ];then uploadFolderToBucket "${AWS_ACCESS_KEY_ID}" "${AWS_SECRET_ACCESS_TOKEN}" "${BUCKET}" "release" "${CI_COMMIT_TAG}"; fi
    - uploadFolderToBucket "${AWS_ACCESS_KEY_ID}" "${AWS_SECRET_ACCESS_TOKEN}" "${BUCKET}" "release" "${BUCKET_PATH}"

release prod:
  <<: *release_definition
  when: manual
  variables:
    DEPLOY_TYPE: prod
    BUCKET: recalbox-archive-stable
    BUCKET_PATH: latest
  only:
    - tags
  environment:
    name: prod
    url: https://download.recalbox.com

release review:
  <<: *release_definition
  when: manual
  variables:
    DEPLOY_TYPE: review
    BUCKET: recalbox-archive-reviews
    BUCKET_PATH: latest
  environment:
    name: review/${CI_COMMIT_REF_SLUG}
    url: https://recalbox-archive-reviews.s3.fr-par.scw.cloud${CI_ENVIRONMENT_SLUG}/index.html